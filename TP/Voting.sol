// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

contract Voting {
    struct Voter {
        bool isRegistered;
        bool hasVoted;
        uint votedProposalId;
    }

    struct Proposal {
        string description;
        uint voteCount;
    }

    enum WorkflowStatus {
        RegisteringVoters,
        ProposalsRegistrationStarted,
        ProposalsRegistrationEnded,
        VotingSessionStarted,
        VotingSessionEnded,
        VotesTallied
    }

    uint public winningProposalId; 

    mapping(address => Voter) public voters; // Mappage des adresses des électeurs vers leurs informations

    Proposal[] public proposals; // Tableau des propositions

    WorkflowStatus public currentStatus; // État actuel du processus de vote

    // Événements pour la journalisation
    event VoterRegistered(address indexed voterAddress);
    event WorkflowStatusChange(WorkflowStatus previousStatus, WorkflowStatus newStatus);
    event ProposalRegistered(uint proposalId);
    event Voted(address indexed voter, uint proposalId);

    // Modificateur pour restreindre l'exécution d'une fonction à un certain état du processus de vote
    modifier onlyDuringStatus(WorkflowStatus expectedStatus) {
        require(currentStatus == expectedStatus, "Invalid workflow status");
        _;
    }

    // Constructeur du smart contract
    constructor() {
        currentStatus = WorkflowStatus.RegisteringVoters; // Initialise l'état du processus de vote à l'enregistrement des électeurs
    }

    // Démarre l'enregistrement des propositions
    function startProposalsRegistration() external onlyDuringStatus(WorkflowStatus.RegisteringVoters) {
        currentStatus = WorkflowStatus.ProposalsRegistrationStarted;
        emit WorkflowStatusChange(WorkflowStatus.RegisteringVoters, currentStatus);
    }

    // Met fin à l'enregistrement des propositions
    function endProposalsRegistration() external onlyDuringStatus(WorkflowStatus.ProposalsRegistrationStarted) {
        currentStatus = WorkflowStatus.ProposalsRegistrationEnded;
        emit WorkflowStatusChange(WorkflowStatus.ProposalsRegistrationStarted, currentStatus);
    }

    // Démarre la session de vote
    function startVotingSession() external onlyDuringStatus(WorkflowStatus.ProposalsRegistrationEnded) {
        currentStatus = WorkflowStatus.VotingSessionStarted;
        emit WorkflowStatusChange(WorkflowStatus.ProposalsRegistrationEnded, currentStatus);
    }

    // Met fin à la session de vote
    function endVotingSession() external onlyDuringStatus(WorkflowStatus.VotingSessionStarted) {
        currentStatus = WorkflowStatus.VotingSessionEnded;
        emit WorkflowStatusChange(WorkflowStatus.VotingSessionStarted, currentStatus);
    }

    // Compte les votes et détermine la proposition gagnante
    function tallyVotes() external onlyDuringStatus(WorkflowStatus.VotingSessionEnded) {
        currentStatus = WorkflowStatus.VotesTallied;
        emit WorkflowStatusChange(WorkflowStatus.VotingSessionEnded, currentStatus);

        uint maxVoteCount = 0;
        uint winningProposalIndex = 0;

        // Parcourt toutes les propositions pour trouver celle avec le plus de votes
        for (uint i = 0; i < proposals.length; i++) {
            if (proposals[i].voteCount > maxVoteCount) {
                maxVoteCount = proposals[i].voteCount;
                winningProposalIndex = i;
            }
        }

        winningProposalId = winningProposalIndex;
    }

    // Enregistre une proposition
    function registerProposal(string memory description) external onlyDuringStatus(WorkflowStatus.ProposalsRegistrationStarted) {
        uint proposalId = proposals.length;

        Proposal memory newProposal;
        newProposal.description = description;
        proposals.push(newProposal);

        emit ProposalRegistered(proposalId);
    }

    // Enregistre un électeur sur la liste blanche
    function registerVoter(address voterAddress) external onlyDuringStatus(WorkflowStatus.RegisteringVoters) {
        require(!voters[voterAddress].isRegistered, "Voter already registered");

        voters[voterAddress].isRegistered = true;

        emit VoterRegistered(voterAddress);
    }

    // Enregistre le vote d'un électeur pour une proposition donnée
    function vote(uint proposalId) external onlyDuringStatus(WorkflowStatus.VotingSessionStarted) {
        require(voters[msg.sender].isRegistered, "Only registered voters can vote");
        require(!voters[msg.sender].hasVoted, "Voter has already voted");
        require(proposalId < proposals.length, "Invalid proposal id");

        voters[msg.sender].hasVoted = true;
        voters[msg.sender].votedProposalId = proposalId;
        proposals[proposalId].voteCount++;

        emit Voted(msg.sender, proposalId);
    }

    // Retourne l'ID de la proposition gagnante
    function getWinner() external view returns (uint) {
        require(currentStatus == WorkflowStatus.VotesTallied, "Voting is not finished yet");
        return winningProposalId;
    }
}