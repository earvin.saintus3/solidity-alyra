// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.8.2 <0.9.0;

contract Test {

    mapping(address => bool) hasExecutedAFunction;
    mapping(address => uint) public moneySent;

    function getCurrentTime() external view returns(uint){
        return block.timestamp;
    }

    function execute() external {
        hasExecutedAFunction[msg.sender] = true;
    }

    function gtIfHasExecutedAFunction() external view returns(bool){
        return hasExecutedAFunction[msg.sender];
    }

    function mint() external payable {
        moneySent[msg.sender] = msg.value;
    }

    function getBalanceOftheContact() external view returns(uint) {
        return address(this).balance;
    }
}