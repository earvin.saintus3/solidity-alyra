// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.8.2 <0.9.0;

contract ExoStructureControle {

    uint number; 

    //Fonction While
    function storeWhile(uint num) public {

        uint increment=num;
        uint limit=12;
        while (limit>increment){
            increment+=num;
        }
        number = increment;
    }
    
    //Function Do While
    function storeDoWhile(uint num) public {
        uint increment=num;
        uint limit=12;
        do {
            increment+=num;
        } while(limit>increment);
        number = increment;
    }

    //Fonction IfElse 
    function storeIf(uint num) public {
        if (num==0){
            number=100;
        }
        else if( num ==1){
            number = 101;
        }
        else {
            number = num;  
        }
    }

    //Fonction For
    // function store(uint num) public {
    //     uint increment=num;
    //     uint limit=12;
    //     for (uint i=0; i<10;i++) {
    //         if (limit == increment){
    //             increment ++;
    //             break;
    //         }
    //         increment+=num;
    //     } 
    //     number = increment;
    // }

    //Fonction for return var
    function store(uint num) public returns (uint) {
        require (num !=0, "Vous ne pouvez pas mettre 0");

        uint increment=num;
        uint limit=12;
        for (uint i=0; i<10;i++) {
            if (limit == increment){
                return increment;
            }
            increment+=num;
        } 

        if(increment>40){
            revert (unicode"vous etes allés trop loin");
        }
        number = increment;
        return number;
    }
    

    //fonction retieve
    function retrieve() public view returns (uint){
        return number;
    }
}