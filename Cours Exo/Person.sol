// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.8.2 <0.9.0;

/**
 * @title Person
 * @dev Store & retrieve value in a variable
 * @custom:dev-run-script ./scripts/deploy_with_ethers.ts
 */
contract People {

    struct Person {
        string name;
        uint age;
    }

    Person public moi;

    //Création d'un fonction modify person

    /*
    function modifyPerson(string memory _name, uint _age) public {
        moi.name = _name;
        moi.age = _age;
    }
    */

    //Création d'un tableau dynamique se nommant persons
    Person[] public persons;

    // Person[0].name;

    //Création d'un fonction pour ajouter et supprimer des persons
    function add(string memory _name, uint _age) public {
        Person memory person = Person(_name, _age);  // création d'un nouveau objet
        persons.push(person); // Ajout de l'objet "Person" dans le tableau
    }
    function remove() public {
        persons.pop(); // Suppression du dernier objet du tableau
    }


}