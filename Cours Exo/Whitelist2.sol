// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.0;

import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/access/Ownable.sol";

contract Whitelist2 is Ownable {
    //création d'un mapping
    mapping(address => bool) public whitelist;
    mapping(address => bool) public blacklist;

    event Authorized(address _address);
    event Banned(address _address);

    function authorized(address _address) public {
        require(!blacklist[_address], "sont des blacklist");
        require(!whitelist[_address], "sont des whitelist");
        whitelist[_address] = true;
        emit Authorized(_address);
    }

    function unauthorized(address _address) public {
        require(!blacklist[_address], "sont des blacklist");
        require(!whitelist[_address], "sont des whitelist");
        blacklist[_address] = true;
        emit Banned(_address);
    }

    function isWhitelisted(address _addr) public view returns (bool) {
        return whitelist[_addr];
    }

    function isBlacklisted(address _addr) public view returns (bool) {
        return blacklist[_addr];
    }
}
