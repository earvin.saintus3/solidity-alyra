// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.0;

contract Random {

    uint nonce;

    /*
    Correction - exercice Gestion aléatoire avec Keccak256
    */
    function random () public returns (uint) {
        nonce++; 
        return uint(keccak256(abi.encodePacked(nonce, block.timestamp))) % 100;
    }


}