// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.0;

contract Whitelist {
    //création d'un mapping
    mapping(address => bool) public whitelist;

    event Authorized(address _address);
    event EthReceived(address _addr, uint value);

    constructor(){

        whitelist[msg.sender]==true;

    }

    modifier check(){
        require(whitelist[msg.sender] == true,
         "tout va bien");
        _;
    }

    function authorized(address _address) public check {

        whitelist[_address] = true;
        emit Authorized(_address);
    }


}
