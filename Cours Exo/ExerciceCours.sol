// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.8.2 <0.9.0;

contract ExerciceCours {}

contract Parent {
    uint variableParent;

    function setMyVar(uint parent) public {
        variableParent = parent;
    }
}

contract Enfant is Parent {
    function getParentVar() public view returns (uint) {
        return variableParent;
    }
}

contract Caller {
    Enfant cc = new Enfant();

    function testInheritance(uint _value) public view returns (uint) {
        // cc.setValue();
    }
}
