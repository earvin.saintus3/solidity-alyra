// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.0;

contract Bank {
    /*
    Correction - Exercice Banque décentralisée
    */
    mapping(address => uint) balances;

    function deposit(uint _amount) public {
        balances[msg.sender] += _amount;
    }

    function transfer(address _to, uint _amount) public {
        require(_to != address(0), "vous ne pouvez burn vos token");
        require(
            balances[msg.sender] >= _amount,
            "vous n'avez pas assez de token"
        );

        balances[msg.sender] -= _amount;
        balances[_to] += _amount;
    }

    function balanceOf(address _addr) public view returns (uint) {
        return balances[_addr];
    }
}
